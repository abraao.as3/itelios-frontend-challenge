import autobind from 'autobind-decorator';
import Selected from './components/Selected';
import CarrousselContainer from './components/CarrousselContainer';

@autobind
class Main{
    @Private _req = new XMLHttpRequest();
    @Private _root = document.createElement('div');
    @Private _center = document.createElement('div');
    @Private _selected = null;
    @Private _carroussel = null;

    @Private _data = null;

    constructor(){
        this._req.addEventListener('load', this._receiveData);
        this._req.responseType = 'json';
        this._req.open('GET', 'products.json');
        this._req.send();

        this._root.id = "root";
        this._center.id = "center";

        this._root.appendChild(this._center);
        document.body.appendChild(this._root);
    }

    @Private
    _receiveData(e){
        this._data = this._req.response[0].data;
        console.log(this._data);

        this._selected = new Selected(this._data.item);
        this._center.appendChild(this._selected.dom);

        this._carroussel = new CarrousselContainer(this._data.recommendation);
        this._center.appendChild(this._carroussel.dom);
    }
}

window.onload = () => {
    new Main();
}
