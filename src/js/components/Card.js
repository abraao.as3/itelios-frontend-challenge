import autobind from 'autobind-decorator';

import Global from '../utils/Global';

class Card{
    @Private _dom = null;
    @Private _image = null;
    @Private _desc = null;
    @Private _price = null;
    @Private _payment = null;
    @Private _button = null;

    constructor(data){
        var key = Global.countCard;
        Global.countCard++;

        this._dom = document.createElement('div');
        this._dom.id = "Card_" + key;
        this._dom.className = "card";

        var imageFilter = /[\w \W]+arquivos\//g;
        var imageURL = data.imageName.replace(imageFilter, "");

        this._image = new Image();
        this._image.className = "image";
        this._image.onload = {} = this._imageloaded;
        this._image.src = imageURL;

        var descArr = data.name.split(" ");
        descArr.splice(13, descArr.length);
        var cutDesc = descArr.join(" ") + " ...";

        this._desc = document.createElement('p');
        this._desc.className = "desc";
        this._desc.innerHTML = cutDesc;

        this._price = document.createElement('h1');
        this._price.className = "price";
        this._price.innerHTML = "<span>Por: </span>" + data.price;

        var paymentFilterInit = "ou ";
        var paymentFilterFinal = " sem juros";

        var paymentDesc = data.productInfo.paymentConditions;
        paymentDesc = paymentDesc.replace(paymentFilterInit, paymentFilterInit + "<span>");
        paymentDesc = paymentDesc.replace(paymentFilterFinal, "</span>" + paymentFilterFinal);

        this._payment = document.createElement('h2');
        this._payment.className = "payment";
        this._payment.innerHTML = paymentDesc;

        this._dom.appendChild(this._image);
        this._dom.appendChild(this._desc);
        this._dom.appendChild(this._price);
        this._dom.appendChild(this._payment);

    }

    get dom(){
        return this._dom;
    }

    @Private
    _imageloaded(){

    }
}

export default Card;
