import autobind from 'autobind-decorator';
import Container from './Container';
import Card from './Card';

@autobind
class CarrousselContainer extends Container{


    constructor(data){
        super();

        for(var i = 0; i < data.length; i++){
            var c = new Card(data[i]);

            this.setContent(c.dom);
        }

        this.dom.className += " carrousselContainer";
        this.setTitle("e talvez se interesse por:");
    }
}

export default CarrousselContainer;
