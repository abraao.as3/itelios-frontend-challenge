import autobind from 'autobind-decorator';
import Container from './Container';
import Card from './Card';

@autobind
class Selected extends Container{
    @Private _card = null;

    constructor(cardData){
        super();

        this.dom.className += " selected";
        this._card = new Card(cardData);

        this.setTitle("Você visitou:");
        this.setContent(this._card.dom);
    }
}

export default Selected;
