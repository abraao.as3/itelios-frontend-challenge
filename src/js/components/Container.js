import autobind from 'autobind-decorator';
import Global from '../utils/Global';

@autobind
class Container{
    @Private _dom = null;
    @Private _title = null;
    @Private _container = null;

    constructor(){
        var key = Global.countContainer;
        Global.countContainer++;

        this._dom = document.createElement('div');
        this._dom.id = "Container_" + key;
        this._dom.className = "container";

        this._title = document.createElement('h1');
        this._title.className = 'title';

        this._container = document.createElement('div');
        this._container.className = 'container';

        this._dom.appendChild(this._title);
        this._dom.appendChild(this._container);
    }

    get dom(){
        return this._dom;
    }

    setTitle(title){
        this._title.innerHTML = title;
    }

    setContent(content){
        this._container.appendChild(content);
    }
}

export default Container;
