module.exports = function(grunt){
    grunt.loadNpmTasks('grunt-chokidar');
    grunt.loadNpmTasks('grunt-tinyimg');
    grunt.loadNpmTasks('grunt-shell');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');

    grunt.initConfig({
        clean: {
            dist: ['dist']
        },

        shell: {
            scripts: {
                command: 'webpack'
            },

            scriptsP: {
                command: 'webpack -p'
            }
        },

        tinyimg: {
            images:{
                files:[{
                    expand: true,
                    cwd: 'src/images',
                    src: ['**/*.{png,jpg,svg}'],
                    dest: 'dist/imagens/'
                }]
            }
        },

        compass: {
            dev: {
                options: {
                    config: 'config.rb'
                }
            }
        },

        copy: {
            products: {
                files: [{
                    expand: true,
                    flatten: true,
                    src: 'src/products.json',
                    dest: 'dist',
                    filter: 'isFile'
                }]
            }
        },

        chokidar: {
            scripts: {
                files: ['src/js/**/*.js'],
                tasks: ['shell:scripts']
            },

            sass: {
                files: ['src/sass/**/*.scss'],
                tasks: ['compass']
            }
        }
    });

    grunt.registerTask('default', ['chokidar']);
    grunt.registerTask('initialize', ['clean', 'shell:scripts', 'tinyimg', 'compass', 'copy', 'chokidar']);
    grunt.registerTask('build', ['clean', 'shell:scriptsP', 'tinyimg', 'compass', 'copy']);
}
