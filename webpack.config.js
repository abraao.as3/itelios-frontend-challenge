const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: './src/js/Main.js',
    output: {
        path: path.resolve(__dirname, 'dist/js'),
        filename: 'script.js'
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                include: [path.resolve(__dirname, "src/")],
                loader: "babel-loader",
                options: {
                    presets: ['env'],
                    plugins: ['babel-plugin-transform-private-properties', 'transform-decorators-legacy', 'transform-class-properties']
                }
            }
        ]
    },

    plugins: [
        new HtmlWebpackPlugin({
            title: 'Itelios',
            inject: 'head',
            filename: '../index.html',
            template: 'src/index.html',
            minify: {
                collapseWhitespace: true
            }
        })
    ]
}
